<?php

namespace App\Console\Commands;

use App\Models\Production;
use App\Models\Runtime;
use App\Services\ImportService;
use Illuminate\Console\Command;
use Monitor\Models\Entity;

class ImportMeasurements extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:measurements';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports all measured data from instance context';

    /**
     * @var ImportService
     */
    protected $importService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( ImportService $importService )
    {
        $this->importService = $importService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        /** @todo We should probably add some logic to not run through each entry over and over, but for our example, it'll do */

        /** @todo We should probably also add some handling for timezones and such, but again, it'll do */

        $measurements = Production::all();
        foreach($measurements as $measurement)
        {
            /** @var Production $measurement */
            $this->importService->importMeasurement($measurement);
        }

        $heartbeats = Runtime::all();
        foreach($heartbeats as $heartbeat)
        {
            /** @var Runtime $heartbeat */
            $this->importService->importHeartbeat($heartbeat);
        }
    }
}