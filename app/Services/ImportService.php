<?php

namespace App\Services;

use App\Models\Production;
use App\Models\Runtime;
use Monitor\Models\Entity;
use Monitor\Repositories\EntityRepository;
use Monitor\Repositories\HeartbeatRepository;
use Monitor\Repositories\MeasurableRepository;
use Monitor\Repositories\MeasurementRepository;
use Monitor\Repositories\PingableRepository;

class ImportService
{
    /**
     * @var EntityRepository
     */
    protected $entityRepository;

    /**
     * @var MeasurableRepository
     */
    protected $measurableRepository;

    /**
     * @var PingableRepository
     */
    protected $pingableRepository;

    /**
     * @var MeasurementRepository
     */
    protected $measurementRepository;

    /**
     * @var HeartbeatRepository
     */
    protected $heartbeatRepository;

    /**
     * ImportService constructor.
     * @param EntityRepository $entityRepository
     * @param MeasurableRepository $measurableRepository
     * @param PingableRepository $pingableRepository
     * @param MeasurementRepository $measurementRepository
     * @param HeartbeatRepository $heartbeatRepository
     */
    public function __construct(
        EntityRepository $entityRepository,
        MeasurableRepository $measurableRepository,
        PingableRepository $pingableRepository,
        MeasurementRepository $measurementRepository,
        HeartbeatRepository $heartbeatRepository
    )
    {
        $this->entityRepository = $entityRepository;
        $this->measurableRepository = $measurableRepository;
        $this->pingableRepository = $pingableRepository;
        $this->measurementRepository = $measurementRepository;
        $this->heartbeatRepository = $heartbeatRepository;
    }

    /**
     * Import measurement from source database
     * @param Production $production
     * @return boolean Succeed or not
     * @throws \Exception
     */
    public function importMeasurement(Production $production):bool
    {
        if(! $production->isValid())
        {
            // Just skip for now
            return false;
        }

        // Since we don't have an UUID available, match on display_name
        $entity = $this->entityRepository->findOrCreateByName($production->machine_name);

        // Get variable by current context
        $variable = $this->measurableRepository->findOrCreateForEntity($entity, $production->variable_name);

        $measurement = $this->measurementRepository
            ->findOrCreateByTimestamp($variable, $production->datetime_from);

        // Fill in measurement values
        $measurement->weight    = $production->weight;
        $measurement->value     = $production->value;

        return $this->measurementRepository
            ->save($measurement);
    }

    /**
     * Import heartbeat from source database
     * @param Runtime $runtime
     * @return boolean Succeed or not
     * @throws \Exception
     */
    public function importHeartbeat(Runtime $runtime):bool
    {
        if(! $runtime->isValid())
        {
            // Just skip for now
            return false;
        }

        // Since we don't have an UUID available, match on display_name
        $entity = $this->entityRepository->findOrCreateByName($runtime->machine_name);

        // Since we have no (or say - just one) ping type, label it to what we know this ping does
        $variable = $this->pingableRepository->findOrCreateForEntity($entity, 'downtime');

        $heartbeat = $this->heartbeatRepository
            ->findOrCreateByTimestamp($variable, $runtime->datetime);

        // Fill in heartbeat values
        $heartbeat->value = !! $runtime->isrunning;

        return $this->heartbeatRepository
            ->save($heartbeat);
    }
}
