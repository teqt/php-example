<?php

namespace App\Services;

use Monitor\Aggregators\HourlyAggregator;
use Monitor\Aggregators\ThresholdAggregator;
use Monitor\Aggregators\TotalAggregator;
use Monitor\Aggregators\UptimeAggregator;
use Monitor\Models\Entity;
use Monitor\Models\Measurable;
use Monitor\Repositories\EntityRepository;
use Monitor\Repositories\MeasurableRepository;

/**
 * Class EntityReportService
 *
 * Reports are most likely custom made, so this service is expected to be
 * customer specific and therefore placed in the application, instead of
 * the package.
 *
 * @package App\Services
 */
class EntityReportService
{
    /**
     * @var EntityRepository
     */
    protected $entityRepository;

    /**
     * @var MeasurableRepository
     */
    protected $measurableRepository;

    /**
     * EntityReportService constructor.
     * @param EntityRepository $entityRepository
     */
    public function __construct(EntityRepository $entityRepository, MeasurableRepository $measurableRepository)
    {
        $this->entityRepository = $entityRepository;
        $this->measurableRepository = $measurableRepository;
    }

    /**
     * Get report for the given timespan
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     */
    final public function production(\DateTime $from, \DateTime $to = null)
    {
        if(is_null($to))
        {
            $to = clone $from;
            $to->add(new \DateInterval('P1D'));
        }

        $report = [];
        $entities = $this->entityRepository->findAll();
        foreach($entities as $entity)
        {
            $report[] = array_merge([
                'machine'       => $entity->display_name,
                'datetime_from' => $from->format('Y-m-d H:i:s'),
                'datetime_to'   => $to->format('Y-m-d H:i:s')
            ], $this->uptime($entity, $from, $to), $this->report($entity, $from, $to));
        }

        return $report;
    }

    final public function status(\DateTime $from, \DateTime $to = null)
    {
        if(is_null($to))
        {
            $to = clone $from;
            $to->add(new \DateInterval('P1D'));
        }

        $temperature = $this->measurableRepository->findOrCreateByCode(self::MEASURABLE_TEMPERATURE);

        $status = [];
        $entities = $this->entityRepository->findAll();
        foreach($entities as $entity)
        {
            $thresholdAggregator = new ThresholdAggregator([85, 100]);
            $measurements = $entity->measurements($temperature, $from, $to);
            foreach ($measurements as $measurement)
            {
                $thresholdAggregator->add($measurement);
            }

            $aggregates = $thresholdAggregator->get();
            $status[ $entity->display_name ] = 'good/green';
            if($aggregates[85] > 0 && $aggregates[85] < 900 && ! $aggregates[100])
            {
                $status[ $entity->display_name ] = 'warning/orange';
            }
            else if($aggregates[85] > 900 || $aggregates[100])
            {
                $status[ $entity->display_name ] = 'fatal/red';
            }
        }

        return $status;
    }

    final protected function report(Entity $entity, \DateTime $from, \DateTime $to)
    {
        $totals = [];
        foreach($entity->measurables() as $measurable)
        {
            if( $measurable->code === self::MEASURABLE_TEMPERATURE )
            {
                continue;
            }

            $totalAggregator = new TotalAggregator();
            $hourlyAggregator = new HourlyAggregator();

            /** @var Measurable $measurable */
            $measurements = $entity->measurements($measurable, $from, $to);
            foreach ($measurements as $measurement)
            {
                $totalAggregator->add($measurement);
                $hourlyAggregator->add($measurement);
            }

            $totals[$measurable->code]['total'] = $totalAggregator->get();
            $totals[$measurable->code]['hourly'] = $hourlyAggregator->get();
        }

        $scrapPercentage = 0;
        if( $totals['production']['total'])
        {
            $scrapPercentage = ($totals['scrap']['total'] / $totals['production']['total']);
        }

        return [
            'scrap_percentage'  => $scrapPercentage,
            'production'        => $this->substractTotals( $totals['production'], $totals['scrap'] )
        ];
    }

    final protected function uptime(Entity $entity, \DateTime $from, \DateTime $to)
    {
        $totals = [];
        foreach($entity->pingables() as $pingable)
        {
            $uptimeAggregator = new UptimeAggregator();

            /** @var Measurable $measurable */
            $heartbeats = $entity->heartbeats($pingable, $from, $to);
            foreach ($heartbeats as $heartbeat)
            {
                $uptimeAggregator->add($heartbeat);
            }

            $totals[ $pingable->code.'_percentage' ] = $uptimeAggregator->get();
        }

        return $totals;
    }

    final protected function substractTotals( $in, $substraction )
    {
        // If either one of the arguments is an array, and the other is not, trigger an exception
        if ((! is_array($in) && is_array($substraction)) ||
            (is_array($in) && ! is_array($substraction)))
        {
            throw new \InvalidArgumentException("Structure of the given arrays do not match.");
        }

        if(! is_array($in))
        {
            return ((float) $in - (float)$substraction);
        }

        $result = [];
        foreach ($in as $key => $value)
        {
            if (! array_key_exists($key, $substraction))
            {
                $result[ $key ] = $value;
                continue;
            }

            $result[ $key ] = $this->substractTotals($value, $substraction[ $key ]);
        }

        return $result;
    }

    public const MEASURABLE_TEMPERATURE = 'core temperature';
}