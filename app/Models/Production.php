<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    /**
     * Table name for model
     * @var string
     */
    public $table = 'Production';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql-src';

    final public function getWeightAttribute()
    {
        return ($this->datetime_to->getTimestamp() - $this->datetime_from->getTimestamp());
    }

    /**
     * Transform datetime_from values to DateTime objects
     * @param $value
     * @return \DateTime
     */
    final public function getDatetimeFromAttribute($value):\DateTime
    {
        return $this->mutateDatetimeField($value);
    }

    /**
     * Transform datetime_to values to DateTime objects
     * @param $value
     * @return \DateTime
     */
    final public function getDatetimeToAttribute($value):\DateTime
    {
        return $this->mutateDatetimeField($value);
    }

    /**
     * Mutate the given value to DateTime object
     * @param $value
     * @return \DateTime
     */
    final protected function mutateDatetimeField($value):\DateTime
    {
        $datetime = new \DateTime($value);
        if(!$datetime->getTimestamp())
        {
            return null;
        }

        return $datetime;
    }

    /**
     * Is current record valid or not
     * @return bool
     */
    final public function isValid():bool
    {
        return ($this->datetime_from && $this->datetime_to);
    }
}