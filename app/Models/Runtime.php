<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Runtime extends Model
{
    /**
     * Table name for model
     * @var string
     */
    public $table = 'Runtime';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql-src';

    /**
     * Transform datetime values to DateTime objects
     * @param $value
     * @return \DateTime
     */
    final public function getDatetimeAttribute($value):\DateTime
    {
        $datetime = new \DateTime($value);
        if(!$datetime->getTimestamp())
        {
            return null;
        }

        return $datetime;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        // Nothing to check on, for now
        return !! $this->datetime;
    }
}