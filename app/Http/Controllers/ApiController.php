<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Services\EntityReportService;
use Illuminate\Support\Facades\Response;

/**
 * Class ApiController
 * @package App\Http\Controllers
 * @todo Errors should be handled more neat
 */
class ApiController extends Controller
{
    /**
     * @var EntityReportService
     */
    protected $entityReportService;

    /**
     * ApiController constructor.
     * @param EntityReportService $entityReportService
     */
    public function __construct(EntityReportService $entityReportService)
    {
        $this->entityReportService = $entityReportService;
    }

    /**
     * Get report for available entities
     * @return \Illuminate\Http\JsonResponse
     */
    public function report()
    {
        $from   = new \DateTime(Input::get('from', null));

        try
        {
            $report = $this->entityReportService->production($from);
            return Response::json($report);
        }
        catch( \Exception $e )
        {
            return Response::json([
                'status'    => 'Error generating your report.',
                'message'   => sprintf("%s in file %s on line %d", $e->getMessage(), $e->getFile(), $e->getLine())
            ], 500);
        }
    }

    public function status()
    {
        $from   = new \DateTime(Input::get('from', null));

        try
        {
            $status = $this->entityReportService->status($from);
            return Response::json($status);
        }
        catch( \Exception $e )
        {
            return Response::json([
                'status'    => 'Error generating your report.',
                'message'   => sprintf("%s in file %s on line %d", $e->getMessage(), $e->getFile(), $e->getLine())
            ], 500);
        }
    }
}
