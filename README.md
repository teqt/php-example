# Laravel example code

## Before we dive into the code.

This project is a round up off an assessment by a company in my network. I have decided to use Laravel due to the broad usage of it, and the lack of experience I had with this framework. Some additional challenge, you might say. The assignment was to create some reporting on variables collected periodically in a database. I am not completely satisfied with every line of code, but for this assessment I was on a time limit.

### My plan of action

Before this assignment started, I decided to come up with a database schema, which allowed me to properly structure my reporting tools. Next, I figured that since this was there assessment, they should probably also have reporting on context outside the scope of this project. That said, I decided to abstract the datamodel given (which was horrible anyway) and make an import command to transform the given datamodel into something better structured.

After creating a suitable database schema, we needed to create reports on this data. I also decided to abstract that part a bit, into aggregators. This solution alows be to later make reports easily configurable from for example database configuration. I will add this later.

**Time currently spend**: 6 hours

I will add some tests, a front-end and do some refactoring later on, but still, any feedback is welcome! Don't hestitate to contact me, at teqt@teqt.nl.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

Just clone this project into any environment you'd like, and use composer to install it:

```shell
~# composer install
```

Next, copy the example configuration .env.example to an actual configuration (.env), and fill in database credentials and a source database. You also need to fill a key, which you can have generated using:

```shell
~# php artisan 
```

After that, You may use artisan to get the database up and running:

```shell
~# php artisan migrate
```

You are now up and running. You can use a console command to import data from the source database to the actual database used for status reporting.

```shell
~# php artisan import:measurements
```

### End result

After importing some actual data, you should have a status up and running. Examples of the results can be found here:

http://example.teqt.nl/api/report?from=2018-01-07%2000:00:00

http://example.teqt.nl/api/status?from=2018-01-07%2000:00:00

## Acknowledgments

* Learned code conventions and best practices of the Laravel framework
* Gave insight in what to expect of my coding skills
* Just had a fun and challenging task to work on.
